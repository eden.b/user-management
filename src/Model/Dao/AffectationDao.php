<?php
namespace DWWM\Model\Dao;

use \PDO;
use DWWM\Model\Dal\Dal;

class AffectationDao extends Dal
{
    public function affect($id_utilisateur, $id_groupe)
    {
        // Requête SQL
        $query = "INSERT INTO `utilisateur_groupe`
                  (`id_utilisateur`, `id_groupe`)
                  VALUES
                  (:id_utilisateur, :id_groupe);";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        $sth->bindParam(":id_utilisateur", $id_utilisateur);
        $sth->bindParam(":id_groupe", $id_groupe);
        // Execution de la requête
        $sth->execute();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
    }

    public function disaffect($id_utilisateur, $id_groupe)
    {
        // Requête SQL
        $query = "DELETE FROM `utilisateur_groupe`
                  WHERE `id_utilisateur` = :id_utilisateur
                  AND `id_groupe` = :id_groupe;";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        $sth->bindParam(":id_utilisateur", $id_utilisateur);
        $sth->bindParam(":id_groupe", $id_groupe);
        // Execution de la requête
        $sth->execute();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
    }
}
